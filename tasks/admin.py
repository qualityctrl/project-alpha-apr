from django.contrib import admin
from tasks.models import Task


@admin.register(Task)
class Task(admin.ModelAdmin):
    list_display = (
        "start_date",
        "due_date",
        "project",
        "assignee",
    )
